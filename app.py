#!/usr/bin/env python3

from flask import Flask, render_template, flash, redirect, request, session, send_from_directory, url_for
from werkzeug.datastructures import ImmutableOrderedMultiDict
import requests
import os, logging, subprocess, urllib.request, csv
from config.config import *
from pathlib import Path
from werkzeug.utils import secure_filename
from lib.functions_general import *


UPLOAD_FOLDER = 'import/'
ALLOWED_EXTENSIONS = set(['csv'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


# Paypal IPN Stuff
@app.route('/ipn/purchase/')
def purchase():
	try:
		return render_template("subscribe.html")
	except Exception as e:
		return str(e)


@app.route('/ipn/', methods=['POST'])
def ipn():
	try:
		arg = ''
		request.parameter_storage_class = ImmutableOrderedMultiDict
		values = request.form
		for x, y in values.iteritems():
			arg += "&{x}={y}".format(x=x, y=y)

		validate_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_notify-validate{arg}'.format(arg=arg)
		r = requests.get(validate_url)
		if r.text == 'VERIFIED':
			try:
				payer_email = request.form.get('payer_email')
				unix = int(time.time())
				payment_date = request.form.get('payment_date')
				username = request.form.get('custom')
				last_name = request.form.get('last_name')
				payment_gross = request.form.get('payment_gross')
				payment_fee = request.form.get('payment_fee')
				payment_net = float(payment_gross) - float(payment_fee)
				payment_status = request.form.get('payment_status')
				txn_id = request.form.get('txn_id')
			except Exception as e:
				with open('/tmp/ipnout.txt', 'a') as f:
					data = 'ERROR WITH IPN DATA\n' + str(values) + '\n'
					f.write(data)

			with open('/tmp/ipnout.txt', 'a') as f:
				data = 'SUCCESS\n' + str(values) + '\n'
				f.write(data)

			conn = sql.connect(db_host, db_uname, db_pass, db_name)
			c = conn.cursor(sql.cursors.DictCursor)
			conn.ping(reconnect=True)
			c.execute("INSERT INTO ipn (unix, payment_date, username, last_name, payment_gross, payment_fee, payment_net, payment_status, txn_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (unix, payment_date, username, last_name, payment_gross, payment_fee, payment_net, payment_status, txn_id))
			conn.commit()
			conn.close()
			gc.collect()

		else:
			with open('/tmp/ipnout.txt', 'a') as f:
				data = 'FAILURE\n' + str(values) + '\n'
				f.write(data)

		return r.text
	except Exception as e:
		return str(e)


@app.route('/ipn/success')
def success():
	try:
		return render_template('success.html')
	except Exception as e:
		return str(e)


# Various API stuff for control panel services
@app.route('/api/<service>')
def api_service(service):
	if not session.get('logged_in'):
		return render_template('login.html')
	else:
		if checkprocess(service) is True:
			data = "Online"
		else:
			data = "Offline"
		return render_template('apidata.html', **locals())


# About Page

@app.route('/about')
def about():
	if not session.get('logged_in'):
		return render_template('login.html')
	else:
		query = config_version
		return render_template('about.html', about=query)


# Login/Logout services
@app.route('/login', methods=['GET', 'POST'])
def do_login():
	if not session.get('logged_in'):
		return render_template('login.html')
	else:
		conn = sql.connect(db_host, db_uname, db_pass, db_name)
		c = conn.cursor(sql.cursors.DictCursor)
		conn.ping(reconnect=True)
		name = request.form['username']
		password = request.form['password']
		c.execute("SELECT * FROM users WHERE username = '%s'" % name)
		for row in c:
			hashed_password = row['password']
			uname = row['username']
			perms = row['permissions']

		if bcrypt.checkpw(password.encode('utf-8'), hashed_password.encode('utf-8')) is True and uname == request.form['username']:
			session['logged_in'] = True
			session['perms'] = perms
			session['username'] = name

		else:
			flash('Wrong Password!')
		conn.close()
		return redirect('/', code=302)


@app.route('/logout')
def logout():
	session.pop('logged_in', None)
	return redirect('/', code=302)


if __name__ == '__main__':
	app.secret_key = os.urandom(12)
	app.run(debug=True, host='0.0.0.0')


