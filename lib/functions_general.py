import time, csv, bcrypt, psutil, requests, json
from config.config import *
import subprocess
from urllib.request import urlopen
from pathlib import Path


# This will generate the password salting
def get_hashed_password(password):
    return bcrypt.hashpw(password.encode('UTF-8'), bcrypt.gensalt())


# This will check plaintext passwording against hashed.
def check_password(password, hashed_password):
    if hashed_password == bcrypt.hashpw(password.encode('utf-8'), hashed_password.encode('utf-8')):
        return True
    else:
        return False


# Check users' permission level
def permissions(username):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT username, permissions FROM editors WHERE permissions = 'caster' and username = '%s'" % username)
    query = c.fetchone()
    conn.close()
    if query is None:
        return True
    else:
        return False


def pid_exists(pid):
	pid = int(pid)
	return psutil.pid_exists(pid)


# Check for various processes and say if they are running
def checkprocess(service):
	p = subprocess.Popen(["ps", "-a"], stdout=subprocess.PIPE)
	out, err = p.communicate()
	if service in str(out):
		return True
	else:
		return False
