import pymysql as sql

from config.database import *

global config, db_name, db_host, db_uname, db_pass

settings = {}	# Create new settings dictionary

conn = sql.connect(db_host, db_uname, db_pass, db_name)
c = conn.cursor(sql.cursors.DictCursor)

c.execute("SELECT * FROM settings")

for row in c:

	param = row['param']
	value = row['value']

	settings[param] = {}
	settings[param] = value

# All configs should follow the format
# configName = settings.get('table.name', "")

config_version = settings.get('config_version', "")
config_domain = settings.get('config_domain', "")
config_paypal_email = settings.get('config_paypal_email', "")


config = {

	# All config details go here.  Follow the format
	# 'config_name': configName,    # Where the right-side config_name matches configName above.

	'config_version': config_version,
	'config_domain': config_domain,
	'config_paypal_email': config_paypal_email,

}

conn.close()
